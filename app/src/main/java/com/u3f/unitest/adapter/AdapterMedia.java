package com.u3f.unitest.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.u3f.unitest.ActivityFullSize;
import com.u3f.unitest.R;
import com.u3f.unitest.model.FileModel;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterMedia extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context c;
    private ArrayList<FileModel> files;

    public AdapterMedia(Context c, ArrayList<FileModel> files) {
        this.c = c;
        this.files = new ArrayList<>(files);

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(c).inflate(R.layout.file_item,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        FileModel fm=new FileModel();
        fm=files.get(position);
        Bitmap myBitmap = BitmapFactory.decodeFile(fm.fileAddress);
        ((ViewHolder) holder).thumb.setImageBitmap(myBitmap);
        ((ViewHolder) holder).tvTitle.setText(fm.title);
        if(fm.isVideo==1){
            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(fm.fileAddress, MediaStore.Video.Thumbnails.MICRO_KIND);
            ((ViewHolder) holder).thumb.setImageBitmap(bMap);
            ((ViewHolder) holder).play.setVisibility(View.VISIBLE);
        }else{
            ((ViewHolder) holder).play.setVisibility(View.GONE);
        }

        ((ViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in =new Intent(c, ActivityFullSize.class);
                in.putExtra("address", files.get(position).fileAddress);
                in.putExtra("type", files.get(position).isVideo);
                c.startActivity(in);
            }
        });
//        ((ViewHolder) holder).tvTitle.setText(fm.title);



    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView thumb;
        ImageView play;
        TextView tvTitle;

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            thumb = itemView.findViewById(R.id.imgThumb);
            play = itemView.findViewById(R.id.imgPlay);

        }

    }
}
