package com.u3f.unitest.model;

import java.io.File;

public class FileModel {

    public String title;
    public String fileAddress;
    public int isVideo;

    public FileModel(String title,String fileAddress,int isVideo){
        this.title=title;
        this.fileAddress=fileAddress;
        this.isVideo=isVideo;
    }
    public FileModel()
    {
        this.title="";
        this.fileAddress="";
    }

}
