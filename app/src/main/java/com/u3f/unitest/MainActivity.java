package com.u3f.unitest;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.u3f.unitest.adapter.AdapterMedia;
import com.u3f.unitest.model.FileModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<FileModel> filePath;
    RecyclerView rcvMain;
    private AdapterMedia am;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        showPickDirDilalog();

    }

    private void findViews() {
        rcvMain = findViewById(R.id.rcvMain);
        GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        rcvMain.setLayoutManager(manager);

    }

    private void showPickDirDilalog() {
        filePath = new ArrayList<>();
        AlertDialog.Builder pickAddress = new AlertDialog.Builder(MainActivity.this);
        pickAddress.setMessage("Please Select Your DIR:");
        pickAddress.setPositiveButton("pick", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getFilePermission();

            }
        });
        pickAddress.show();
    }

    private void getFilePermission() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null) {
            Uri u = data.getData();

            String dir = u.getPath();
            dir = dir.substring(dir.indexOf(':') + 1, dir.length());

            File f = new File(dir);

            String path = Environment.getExternalStorageDirectory().toString() + f.getAbsolutePath();
            Log.d("Files", "Path: " + path);
            File directory = new File(path);
            File[] files = directory.listFiles();
            isImageOrVideo(files);
        }

    }

    private void isImageOrVideo(File[] files) {
        for (int i = 0; i < files.length; i++) {


            FileModel fm = new FileModel();
            if (files[i].getName().toLowerCase().contains(".png") || files[i].getName().toLowerCase().contains(".jpg") || files[i].getName().toLowerCase().contains(".jpeg")) {
                fm = new FileModel(files[i].getName(), files[i].getAbsolutePath(), 0);
                filePath.add(fm);

            } else if (files[i].getName().toLowerCase().contains(".mp4")) {
                fm = new FileModel(files[i].getName(), files[i].getAbsolutePath(), 1);
                filePath.add(fm);

            }

        }
        am = new AdapterMedia(MainActivity.this, filePath);
        rcvMain.setAdapter(am);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {


                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    startActivityForResult(intent, 1000);
                } else {
                    getFilePermission();
                    Toast.makeText(MainActivity.this, "We Really Need this permission", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.pick_dir, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.pickDir:
                getFilePermission();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
