package com.u3f.unitest;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityFullSize extends AppCompatActivity {
    ImageView imgFullSize;
    VideoView vidFullSize;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_content);
        Intent in=getIntent();
        String loc=in.getStringExtra("address");
        int isVideo=in.getIntExtra("type",0);
        findViews();
    }

    private void findViews() {
        imgFullSize = findViewById(R.id.imgFullSize);
        vidFullSize = findViewById(R.id.vidFullSize);
    }
}
